<?php
/*
Template Name:Contacto
*/
?>   
        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>

  <section>
    <div class="c-elastic c-mbot-sm">
      <div id="map" class="c-map2"></div>
    </div>
  </section>

  <section>
    <div class="container">

      <div class="row c-row-centered c-mtop-sm ">
        <div class="col-sm-10 col-md-8 col-lg-6 c-col-centered ">
<!--          
          <form method="post" action="" class="text-left">
            <div class="form-group">
              <input type="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="" placeholder="Teléfono">
            </div>
            <div class="form-group">
              <textarea name="" id="" rows="7" class="form-control" placeholder="Mensaje"></textarea>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox"> <small>Deseo subscribirme al boletín y recibir información de los proyectos de Codeín</small>
              </label>
            </div>
            <div class="form-group clearfix">
              <button type="submit" class="btn c-bg-sklight c-color-white c-titi-sem">ENVIAR</button>
            </div>
          </form>
-->

<?php echo do_shortcode( '[contact-form-7 id="4" title="Contactenos"]' ); ?>

        </div>
        <div class="col-sm-10 col-md-3 col-lg-3 c-col-centered c-contactinfo text-left">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-12">
              <h3 class="c-h3 c-titi-sem hidden-md hidden-lg c-mbot-xs">CONTACTO</h3>
              <ul class="list-unstyled">
                <li><a href="tel:14561327"><i class="fa fa-phone"></i> <?php echo get_field("telefonos"); ?></a></li>
                <li><a href="mailto:<?php echo get_field("mail"); ?>"><i class="fa fa-at"></i> <?php echo get_field("mail"); ?></a></li>
                <li><a href="https://goo.gl/maps" target="_blank"><i class="fa fa-map-marker"></i> <?php echo get_field("direccion"); ?></a></li>
              </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-12 c-mbot-sm">
              <h3 class="c-h3 c-titi-sem c-mbot-xs">SÍGUENOS EN</h3>
              <div class="c-follow c-mbot-xs">
                <a href="<?php echo get_field("facebook"); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/btn-facebook.png" alt=""></a>
              </div>
              <div class="c-follow c-mbot-xs">
                <a href="<?php echo get_field("youtube"); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/btn-youtube.png" alt=""></a>
              </div>
              <div class="c-follow c-mbot-xs">
                <a href="<?php echo get_field("linkedin"); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/btn-link.png" alt=""></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<script>
  function initMap() { 
    var myLatLng = {lat: -12.1228, lng: -76.9907404};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: myLatLng
    });
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: "<?php echo get_template_directory_uri(); ?>/img/marker.png",
      title: 'Visitanos',
      animation: google.maps.Animation.DROP,
    });
  }
</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6w1tQpuDMZffD-cPP6y5cTXw42rHF_Ag&callback=initMap">
</script>

<?php /**********************FOOTER***************************/ ?>



            <?php get_footer(); ?>



<?php /**********************ENDFOOTER***************************/ ?>