    <footer class="c-bg-blackt1">
      <div class="container">
                  <?php query_posts(array( 
                                'post_type' => 'page',
                                'name' => 'pie-de-pagina'

                            ) ); 
                          
                        while (have_posts()) : the_post(); 
                        ?>         
        <div class="row c-mtop-md">
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <p class="c-h4 c-titi-sem">LA EMPRESA</p>
            <div class="c-text-justify c-mbot-xs c-lato-lig c-lheight-lg">
              <?php echo get_field('empresa'); ?>
            </div>
            <a href="#" class="c-link">LEER MÁS</a>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <p class="c-h4 c-titi-sem">ATENCIÓN AL CLIENTE</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="<?php echo site_url() ?>/terminos-y-condiciones/">Términos y Condiciones</a></li>
              <li><a href="<?php echo site_url() ?>/politicas-de-privacidad/">Políticas de Privacidad</a></li>
            </ul>
            <p class="c-h4 c-titi-sem c-mtop-sm">TIPOS DE INVERSIÓN</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="<?php echo site_url() ?>/proyectos-actuales/">Venta de departamentos</a></li>
              <li><a href="<?php echo site_url() ?>/invierte-en-coedin/">Compra de casas y terrenos</a></li>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <div class="c-foot-invierte">
              <p class="c-h3 c-color-sklight c-titi">INVIERTE CON <br><span class="c-h2 c-lheight-sm">COEDIN</span></p>
              <div class="c-text-justify c-lato-lig">
                <small class="c-color-white"><?php echo get_field('invierte_coedin'); ?></small>
              </div>
              <p class="c-mtop-xs"><a href="<?php echo site_url(); ?>/invierte-en-coedin/" class="btn btn-block c-bg-sklight c-titi">INVIERTE<br><span class="c-h4 c-titi-sem">AHORA</span></a></p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md c-fdatos">
            <p class="c-h4 c-titi-sem">CONTÁCTANOS</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="https://www.google.com.pe/maps/place/Calle+Segovia+194,+Santiago+de+Surco+15038/@-12.1228817,-76.9912922,17.75z/data=!4m5!3m4!1s0x9105c7f388dca95f:0x915f0860bc80fb9!8m2!3d-12.1227802!4d-76.9907518?hl=es-419" target="_blank"><i class="fa fa-map-marker c-color-white"></i> <span><?php echo get_field('direccion') ?></span></a></li>
              <li><a href="mailto:<?php echo get_field('mail') ?>"><i class="fa fa-envelope c-color-white"></i> <span><?php echo get_field('mail') ?></span></a></li>
            </ul>
            <p class="c-h4 c-titi-sem c-mtop-sm">SÍGUENOS EN</p>
            <div class="c-redes c-mtop-xs">
              <div class="c-iblock">
                <a href="<?php echo get_field('facebook') ?>" class="fb" target="_blank"><i class="fa fa-facebook"></i> </a>
              </div>
              <!--<div class="c-iblock">
                <a href="<?php echo get_field('youtube') ?>"><i class="fa fa-youtube-play"></i> </a>
              </div>
              <div class="c-iblock">
                <a href="<?php echo get_field('linkedin') ?>"><i class="fa fa-linkedin"></i> </a>
              </div>-->

            </div>
          </div>
        </div>
                        <?php endwhile;?>
                        <?php wp_reset_query(); ?>  

      </div>

      <div class="c-foot c-bg-blackt1">
        <div class="container text-center">
          <small class="c-color-sklight">Copyright &copy; 2016 <strong>Coedin</strong> | Diseñado y Desarrollado por <strong>Reder Design</strong></small>
        </div>
      </div>
    </footer>


    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
    <script src="<?php echo  get_template_directory_uri() ?>/js/plugins.js"></script>
    <script type="text/javascript">
      $('.c-menu a').each(function() {
        var word = $(this).html();
        var index = word.indexOf(' ');
        if(index > -1) {
          // index = word.length;
          $(this).html(word.substring(0, index) + '<br>' + word.substring(index, word.length));
        }
      });
    </script>
    <?php wp_footer(); ?> 
  </body>
</html>