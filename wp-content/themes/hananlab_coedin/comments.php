          <div class="c-comentarios c-mtop-md">
            <h3 class="c-h1 c-titi-sem c-mbot-xs">Comentarios</h3>

		<?php if ( have_comments() ) : ?>
			<a href="#commentform" class="pull-right">
				<i class="fa fa-pencil" aria-hidden="true"></i> &nbsp; &nbsp; Deja un Comentaio
			</a>
			<h2 class="comments-title">
				<?php echo get_comments_number(); ?> Comentarios
			</h2>			
			<ol class="comment-list row">
				<?php
					wp_list_comments( 'type=comment&callback=mytheme_comment' );
				?>
			</ol><!-- .comment-list -->							
		<?php endif; // have_comments() ?>		

		<?php
			if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
		?>
			<p class="no-comments"><?php _e( 'Comments are closed.', 'pertel' ); ?></p>
		<?php endif; ?>

		<?php 

		$fields   =  array(
		'author' => '<div class="hidden-xs col-sm-2 col-md-2 c-mbot-xs c-userpic">
                		<figure><img src="'.get_template_directory_uri().'/img/usern.jpg" alt=""></figure>
              		</div>
              		
              			<div class="form-group">
                    		<input id="author" name="author" type="text" value="" aria-required="true" required="required" class="form-control" placeholder="Nombre">
                  		</div>',
		'email'  => '<div class="form-group">
						<input id="email" name="email" type="text" value="" aria-describedby="email-notes" aria-required="true" required="required" class="form-control" placeholder="Email">
					</div>',
		
	);

			$comments_args = array(
		        // change the title of send button 
		        'label_submit'=>'DEJA UN COMENTARIO',
		        // change the title of the reply section
		        'title_reply'=>'Déjanos un comentario',
		        // remove "Text or HTML to be displayed after the set of comment fields"
		        'comment_notes_before' => '',
		        // redefine your own textarea (the comment body)
		        'fields' => $fields,
		        'comment_field' => '<div class="form-group">
		        						<textarea name="comment" id="comment" rows="7" class="form-control" placeholder="Comentario"></textarea>
		        					</div> 
		        				</div>',

			);

			comment_form($comments_args);


		 ?>		
	</div>


