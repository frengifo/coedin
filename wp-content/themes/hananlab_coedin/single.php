        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>

  <section class="single-news">
    <div class="container">
      <div class="c-mbot-sm c-img-bgcover c-coverslide" style="background-image: url(<?php the_post_thumbnail_url( 'full' ) ?>);">
      </div>
    </div>

    <div class="container c-notideta">
      <div class="row">
        <div class="col-md-9">
          <h1><?php the_title(); ?></h1>
          <hr class="c-hr">
          <div class="clearfix c-inline-float c-mbot-sm text-center">
            <div class="c-share c-color-white">
              <div class="c-face"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook"></i></a></div>
              <div class="c-twit"><a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>"><i class="fa fa-twitter"></i></a></div>
              <div class="c-goog"><a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="fa fa-google-plus"></i></a></div>
            </div>
          
            <div class="c-autor c-mtop-xs">
              Por <span class="c-color-sklight"><?php the_author(); ?> </span> <span class="c-separador">|</span> <?php the_date(); ?>
            </div>
          </div>

<?php the_content(); ?>

          <div class="clearfix c-share-bottom">
            <div class="c-share text-center c-color-white c-block c-mtop-sm c-mbot-md">
              <div class="c-face"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook"></i></a></div>
              <div class="c-twit"><a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>"><i class="fa fa-twitter"></i></a></div>
              <div class="c-goog"><a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="fa fa-google-plus"></i></a></div>
            </div>
          </div>

          <div class="c-autorinfo">
            <div class="row">
              <div class="col-sm-2 col-md-2 c-mbot-xs c-userpic">
                <!-- <figure><img src="img/user1.png" alt=""></figure> -->
                <figure><?php echo get_avatar( get_the_author_meta('email'), '103' ); ?></figure>
              </div>
              <div class="col-sm-10 col-md-10 c-mbot-xs">
                <span class="c-h4 c-titi-sem c-block"><?php the_author(); ?></span>
                <span class="c-mbot-xs c-block"><?php the_date(); ?></span>
                <p><small><?php the_author_meta('description'); ?></small></p>
              </div>
            </div>
          </div>

          <?php 

          if ( comments_open() || get_comments_number() ) :
            comments_template( '/comments.php' );
          endif;

          ?>          
        

        

        <div class="col-md-3">
          <h3 class="c-h1 c-titi-sem hidden-md hidden-lg">Buscar</h3>
          
          <div class="c-buscar">
            <form class="find-post" method="get" action="<?php echo site_url(); ?>/index.php/searchpage/">
              <div class="form-group">
                <div class="input-group">
                  <input type="search" name="search" class="form-control" placeholder="Buscar Noticias">
                  <span class="input-group-addon">
                    <button type="submit" class="input-group-addon"></button>
                  </span> 
                  
                </div>
              </div>
            </form>
          </div>
          <hr class="c-hr">
  
          <div class="row c-mtop-sm">

                      
          <?php           
                query_posts(array( 
                    'post_type' => 'post',
                    'showposts' => 3
                    //'orderby' => 'DES'
                ) ); 
                $j=0;
              
            while (have_posts()) : the_post(); 
            ?>
            
            <div class="col-sm-12 col-md-12 c-mbot-sm col-xs-12">
              <div class="c-notihome c-notipo2 c-img-bgcover" style="background-image: url(<?php the_post_thumbnail_url( 'full' ) ?>);">
                <div class="c-noti-text c-color-blackt1 c-text-shadow2" style="background-color:<?php echo hex2rgba(get_field('color_fondo'),get_field('opacidad')) ?>">
                  <p><small><?php echo get_the_date(); ?></small></p>
                  <!-- <h4><?php the_title(); ?></h4> -->
                  <div><?php echo get_field('descripcion'); ?></div>
                  <div class="clearfix">
                    <a href="<?php the_permalink(); ?>" class="btn c-bor-gray pull-right">Leer más</a>
                  </div>
                </div>
              </div>
            </div> 
          <?php $j++; ?>
          <?php endwhile;?>
          <?php wp_reset_query(); ?>              
<!--
            <div class="col-sm-6 col-md-12 c-mbot-sm">
              <div class="c-notihome c-notipo1">
                <figure><img src="img/noti-mas.png" alt=""></figure>
                <div class="c-noti-text c-bg-graylight">
                  <p><small>19 Julio, 2016</small></p>
                  <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
                  <div class="clearfix">
                    <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-12 c-mbot-sm">
              <div class="c-notihome c-notipo1">
                <figure><img src="img/noti-mas.png" alt=""></figure>
                <div class="c-noti-text c-bg-graylight">
                  <p><small>19 Julio, 2016</small></p>
                  <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
                  <div class="clearfix">
                    <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
                  </div>
                </div>
              </div>
            </div>
-->            
          </div>

        </div>


      </div>
    </div>
  </section>
  
<?php get_footer(); ?>