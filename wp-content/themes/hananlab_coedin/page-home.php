<?php
/*
Template Name:Home
*/
?>   
        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>
  <style type="text/css">
    
  .grid-sizer{
    width: 25%;
  }
  .c-mbot-sm-noti{
    height: 580px;
  }
.c-notihome {
    position: relative;
    height: 100%;
    display: block;
    background: #f4f4f4;
    width: 100%;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 50% 50%;

}
  .c-notihome .c-noti-text {
    position: absolute;
    z-index: 10;
    padding: 1em;
    width: 100%;
    left: 0;
    bottom: 0;
    text-shadow:none;
    background-color: rgba(244, 244, 244, 1);
  }


  @media screen and (max-width: 1200px) {
    .c-mbot-sm-noti{
      font-size: 1em;
      height: 480px;
    }
  }
  @media screen and (max-width: 768px) {
    /* 5 columns for larger screens */
    .grid-sizer { width: 50%; }
    .c-mbot-sm-noti{
      height: 430px;
    }
  }

   @media screen and (max-width: 480px) {
    /* 5 columns for larger screens */
    .grid-sizer, .c-mbot-sm-noti { width: 100%; }
    .c-mbot-sm-noti{
      height: 400px;
    }
  }
  </style>        

  <section class="slider">
    <div class="c-elastic">

      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
       
        <!-- Wrapper for slides -->
        <div class="carousel-inner slider-box" role="listbox">
            <ul>
              <?php 

                  query_posts(array( 
                      'post_type' => 'slider',
                      'showposts' => 10,
                      'orderby' => 'ASC'
                  ) ); 
                  $j=0;
                
              while (have_posts()) : the_post(); 
              ?>

                    <li class="item">
                      <div class="c-mbot-sm c-img-bgcover c-top-slide" style="background-image: url(<?php echo get_field("imagen"); ?>);">
                        <div class="c-layer-fosc visible-xs"></div>
                        <div class="c-box-text c-color-white">
                          <p class="c-h2"><strong><?php echo get_field("titulo"); ?></strong></p>
                          <p class="c-h3-4"><?php echo get_field("subtitulo"); ?></p>
                          <a href="<?php echo get_field("link"); ?>" class="btn btn-lg c-btn-skfosc-white c-titi-sem c-mtop-xs"><?php echo get_field("boton"); ?></a>
                        </div>
                      </div>
                    </li>

              <?php $j++; ?>
              <?php endwhile;?>
              <?php wp_reset_query(); ?>  
            </ul>
        </div>
        
      </div>

      
      <div class="c-iblock-container hover-proyectos">
        <div class="c-iblock c-slide60 c-mbot-sm c-mder-xsr c-img-bgcover" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/proyectos-actuales-home.jpg);">
          <div class="c-box-text c-color-white">
            <a href="<?php echo site_url(); ?>/proyectos/actuales/" class="btn btn-lg c-btn-black-sk c-titi">PROYECTOS<span class="c-h4">ACTUALES</span></a>
          </div>
        </div>
        <div class="c-iblock c-slide40 c-mbot-sm c-img-bgcover" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/proyectos-entregados-home.jpg);">
          <div class="c-box-text c-color-white">
            <a href="<?php echo site_url(); ?>/proyectos/entregados/" class="btn btn-lg c-btn-black-green c-titi">PROYECTOS<span class="c-h4">ENTREGADOS</span></a>
          </div>
        </div>
      </div>
    </div>

    <div class="c-elastic">
      <div class="c-bg-blackt1 c-suscribe c-mbot-sm">
        <div class="container ">
          <span class="c-h4 c-lato-lig">Suscríbete a Nuestro Boletín</span>
<!--
          <form class="form-inline ">
            <div class="form-group">
              <input type="email" class="form-control" id="" placeholder="Déjanos tu email">
            </div>
            <button type="submit" class="btn c-bg-sklight c-titi-sem">Enviar</button>
          </form>       
-->
<?php echo do_shortcode( '[contact-form-7 id="191" title="Suscribete"]' ); ?>
        </div>
      </div>
    </div>

    <div class="c-elastic invierte">
      <div class="c-mbot-sm c-img-bgcover c-bot-slide" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/invierte-nosotros.jpg);">
        <div class="container">
          <div class="c-box-text c-color-white">
            <span class="c-h2 c-block c-titi">INVIERTE CON</span>
            <span class="c-h1xx c-block c-titi-sem c-lheight-sm c-mbot-xxs">COEDIN</span>
            <p class="c-mver-xs c-lato-lig"><?php echo get_field("invierte"); ?></p>
            <a href="<?php echo get_field("link"); ?>" class="btn btn-lg c-btn-sklight-white btn-wide-sm c-titi c-mtop-xxs">INVIERTE<br><span class="c-h4 c-titi-sem">AHORA</span></a>
          </div>
        </div>
      </div>
    </div>
    
  </section>

  <section class="news">
    <div class="container">
      <h2 class="text-center c-titi-sem">NOTICIAS RELACIONADAS</h2>
      <div class="row c-mtop-sm">

          <?php  

          query_posts('post_type=post&post_status=publish&posts_per_page=4'); 
          $j=0;

          while ( have_posts() ) : the_post(); 
          ?>

                  <div class="col-sm-6 col-md-3 c-mbot-sm c-mbot-sm-noti col-xs-6">
                    <div class="c-notihome c-notipo2 c-img-bgcover" style="background-image: url(<?php the_post_thumbnail_url( 'full' ) ?>);">
                      <div class="c-noti-text c-color-blackt1 c-text-shadow2" style="background-color:<?php echo hex2rgba(get_field('color_fondo'),get_field('opacidad')) ?>">
                        <p><small><?php echo get_the_date(); ?></small></p>
                        <!-- <h4><?php the_title(); ?></h4> -->
                        <div><?php echo get_field('descripcion'); ?></div>
                        <div class="clearfix">
                          <a href="<?php the_permalink(); ?>" class="btn c-bor-gray pull-right">Leer más</a>
                        </div>
                      </div>
                    </div>
                  </div>  

                <?php endwhile; ?>       
      
     
      </div>
    </div>
  </section>
  
<?php /**********************FOOTER***************************/ ?>



            <?php get_footer(); ?>



<?php /**********************ENDFOOTER***************************/ ?>