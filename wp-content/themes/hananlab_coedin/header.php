<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>COEDIN - Construcción y Ventas Inmobiliarias</title>
    <script src="//use.fontawesome.com/416dbba35a.js"></script>
    <!-- <link href="bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet"> -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/main.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css?v=2" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/parche.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700|Titillium+Web:400,600,700" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri() ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png"/>    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-80367781-2', 'auto');
      ga('send', 'pageview');

    </script>
<?php wp_head(); ?>  

  </head>

  <body>
    <header>
                  <?php query_posts(array( 
                                'post_type' => 'page',
                                'name' => 'encabezado-de-pagina'
                            ) ); 
                          
                        while (have_posts()) : the_post(); 
                        ?>        
      <div class="c-head c-bg-blackb1">
        <div class="container clearfix">
          <ul class="list-inline pull-left">
            <li><a href="<?php echo get_field("facebook"); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="<?php echo get_field("youtube"); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
            <li><a href="<?php echo get_field("linkedin"); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
          </ul>
          <ul class="list-inline pull-right">
            <li><a href="tel:987654321">Comunícate al <strong><i><?php echo get_field("telefono"); ?></i></strong></a></li>
          </ul>
        </div>
      </div>
                        <?php endwhile;?>
                        <?php wp_reset_query(); ?>  


      <div class="navbar-static-top c-header">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="<?php echo site_url(); ?>" class="c-logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Coedin"></a>
            </div>
<!--
            <div class="collapse navbar-collapse" id="navbar-ex-collapse">
               <ul class="nav navbar-nav navbar-right c-menu c-titi-sem">
                  <li class="active"><a href="proyectos-actuales.php">PROYECTOS ACTUALES</a></li>
                  <li><a href="proyectos-entregados.php">PROYECTOS ENTREGADOS</a></li>
                  <li><a href="invierte-en-coedin.php">INVIERTE CON COEDIN</a></li>
                  <li><a href="noticias.php">NOTICIAS</a></li>
                  <li><a href="contactanos.php">CONTACTO</a></li>
               </ul>
            </div>
          -->
<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 
                          'container_id' => 'navbar-ex-collapse', 
                          'container_class' => 'collapse navbar-collapse', 
                          'menu_class' => 'nav navbar-nav navbar-right c-menu c-titi-sem' ) ); ?>    

         </div>
      </div>
    </header>
    <!-- CAMBIO -->
    <div>
      <div class="c-elastic">
      <hr class="c-hr-top">
      </div>
    </div>
    <!-- FIN CAMBIO -->    