        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>

  <section class="banner">
    <div class="c-elastic">
                  <?php query_posts(array( 
                                'post_type' => 'cabecera',
                                'name' => 'proyectos-detalle'

                            ) ); 
                          
                        while (have_posts()) : the_post(); 
                        ?>        
      <div class="c-mbot-sm c-img-bgcover c-coverslide" style="background-image: url(<?php echo get_field("imagen"); ?>);">
      </div>
                        <?php endwhile;?>
                        <?php wp_reset_query(); ?>        
    </div>
  </section>

  <section class="single-proyecto">
    <div class="container">
      <div class="clearfix c-idioma">
        <div class="pull-right">
          <span class="c-titi-bol">Idioma:</span> 
          <a href="?lang=es" class="<?php if(qtrans_getLanguage()=='es'){ echo 'activo'; } ?>">
            <?php if(qtrans_getLanguage()=='en'){ echo 'Spanish'; }else{ echo 'Español';} ?>
          </a> 
          <span class="c-titi-sem">|</span> 
          <a href="?lang=en" class="<?php if(qtrans_getLanguage()=='en'){ echo 'activo'; } ?>">
            <?php if(qtrans_getLanguage()=='en'){ echo 'English'; }else{ echo 'Inglés';} ?>
          </a>
        </div>
      </div>

      <div class="row c-proy-deta c-mtop-sm">
        <div class="col-md-5 c-mbot-sm c-lato-lig">
          <h1 class="c-titi-sem c-mtop-0"><?php the_title(); ?></h1>
          <article>
            <?php the_content(); ?>
          </article>
        </div>
        <div class="col-md-7 c-mbot-sm c-proy-detaimg">
          <div class="row">
            <div class="col-sm-2 col-xs-2 c-detaimg">
              <div class="row">
                
                <div class="col-md-12">
                  <div class="thumb">
                    <?php $fulls = $dynamic_featured_image->get_featured_images(get_the_id()) ?>
                    <?php foreach ($fulls as $key => $items): ?>                   
                        <div>
                          <figure><img src="<?php echo $items['thumb'] ?>" title="<?php the_title(); ?>"></figure>
                        </div>
                    <?php endforeach ?> 
                  </div>
                </div>  

              </div>
            </div>
            <div class="col-sm-10 col-xs-10">
              
                <div class="full">

                  <?php $fulls = $dynamic_featured_image->get_featured_images(get_the_id()) ?>

                  <?php foreach ($fulls as $key => $items): ?>                   
                      
                        <div>
                          <figure><img class="mainimg" src="<?php echo $items['full'] ?>" alt=""></figure>
                        </div>
                     
                  <?php endforeach ?> 

                </div>  

            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

<!-- -->

  <?php $estado = get_the_terms( $post->ID, 'proyectos'); ?>
<?php if ($estado[0]->slug == "actuales"){ ?>
<section class="departamentos">
    <div class="container">


      
      <div class="clearfix c-mbot-xs">
        <h2 class="pull-left c-titi-sem c-mtop-0 map-title">Mapa de Ubicación</h2>
        <div class="pull-right">
          <a href="#solicitar" class="btn c-bg-sklight c-color-white btn-solicitar various"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Solicitar Información de Ubicación</a>
        </div>
      </div>
      <div class="hidden">
        
        <div id="solicitar" class="invierte-form">
          <?php echo do_shortcode( '[contact-form-7 id="222" title="Solicitar Informacion"]' ); ?>
        </div>

      </div>
      <div class="c-proy-map c-mbot-sm">
        <?php $locations = get_post_meta($post->ID,'employeeDetails',true); ?>
        <?php //var_dump($locations); ?>
       
        <div id="map" class="c-map"></div>
      </div>

      <div class="c-proy-serv c-titi-sem">
        <h4>Servicios Aledaños</h4>
        <div class="row c-mtop-xs">
          <?php $serv_uniques = array(); $o = 0; ?>
          <?php foreach ($locations as $key => $value): ?>   
            <?php $serv_uniques[$o] = (int)$value['serv']; $o++; ?>   
          <?php endforeach; ?> 
          <?php $serv_uniques = array_unique($serv_uniques); ?>
          <?php foreach ($serv_uniques as $key): ?>   
            <div class="col-xs-6 col-sm-3 col-md-3 c-mbot-xxs">
              <a  href="javascript:;" onclick="showOnlyMarkers(<?php echo $key; ?>);" class="view-markers">
                
                <i class="fa fa-circle-thin hidden-xs"></i>
                <figure><img src="<?php echo get_field('icono', 'servicios_' . $key); ?>" alt="Servicio"></figure>
                <span>
                  <?php $term = get_term_by( 'id', $key, 'servicios' ); ?>
                  <?php echo $term->name; ?>
                </span>

              </a>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
     
    <!-- -->

      <div class="c-block c-mtop-md">
        <div class="c-iblock-container c-tipo-btns">
    <?php 
          $departamento = new WP_Query( array( 'post__in ' => get_field("depa"), 'post_type' => 'departamento', 'order'=> 'ASC', 'orderby'=> 'date' ) ); 
          $j=0; 
          while ($departamento->have_posts()) : $departamento->the_post();           
    ?>  
      <?php $clsActivo = $j == 0 ? "activo":""; ?>
          <div class="c-iblock c-mbot-xs">
            <a href="#dpto<?php echo $j; ?>" aria-controls="dpto<?php echo $j; ?>" role="tab" data-toggle="tab" class="btn c-titi-sem <?php echo $clsActivo; ?>"><?php the_title(); ?></a>
          </div>
    <?php $j++ ?>
    <?php endwhile;?>
    <?php wp_reset_query(); ?>  
     
        </div>
      </div>
      <!-- BOX -->
      <!--<section> -->
      <div class="c-tipo-box c-mtop-xs">
        <div class="">

    <?php 
          $departamento = new WP_Query( array( 'post__in ' => get_field("depa"), 'post_type' => 'departamento', 'order'=> 'ASC', 'orderby'=> 'date' ) ); 
          $j=0; 

          while ($departamento->have_posts()) : $departamento->the_post();           
    ?>
         
          <div role="tabpanel" class="tab-pane fade <?php if($j==0){echo 'in active';} ?>" id="dpto<?php echo $j; ?>">
            <div class="row">
             <div class="col-sm-6 col-xs-10 right-mobile">
                <div class="full">
                  <div><figure><img src="<?php the_post_thumbnail_url( 'full' ) ?>" alt="" ></figure></div>
                  <?php foreach ($fulls as $key => $items): ?>
                
                    <div><figure><img src="<?php echo $items['full'] ?>" alt="<?php the_title(); ?>" ></figure></div>
                
                  <?php endforeach ?> 
                </div>
             </div>
             
             <div class="col-sm-6 col-xs-8 c-mbot-sm only-desktop">
                <h2 class="c-color-sklight c-titi-sem c-mtop-0"><?php the_title(); ?></h2>
                <p class="c-titi c-h3"><i>ÁREA TOTAL <?php echo get_field('area'); ?>m<sup>2</sup></i></p>
                <div class="c-proy-desc">
                  <article>
                    <?php the_content(); ?>
                  </article>
                </div>
             </div>

             <div class="col-md-12 col-sm-2 col-xs-2 box-thumb">
                

                  <div class="thumb">
                      <div><figure><img src="<?php the_post_thumbnail_url( 'thumb' ) ?>" alt="" ></figure></div>
                      <?php foreach ($fulls as $key => $items): ?>
                    
                        <div><figure><img src="<?php echo $items['thumb'] ?>" alt="<?php the_title(); ?>" ></figure></div>
                    
                      <?php endforeach ?>  
                  </div>
                

              </div>
              <div class="clear"></div>
              <div class="col-sm-12 col-xs-12 only-mobile">
                <h2 class="c-color-sklight c-titi-sem c-mtop-0"><?php the_title(); ?></h2>
                <p class="c-titi c-h3"><i>ÁREA TOTAL <?php echo get_field('area'); ?>m<sup>2</sup></i></p>
                <div class="c-proy-desc">
                  <article>
                    <?php the_content(); ?>
                  </article>
                </div>
             </div>

            </div>

            
          </div>

    <?php $j++ ?>
    <?php endwhile;?>
    <?php wp_reset_query(); ?>  
        </div>
      </div>
 


    <!-- </section> -->         
    <!-- END BOX -->

        <div class="text-center c-mtop-xs c-mbot-sm">
          <a href="#contactar" class="btn btn-lg c-bg-sklight c-titi-sem c-color-white btn-contactar various">CONTACTAR AHORA</a>
        </div>
        <div class="hidden">
        
        <div id="contactar" class="invierte-form">
          <?php echo do_shortcode( '[contact-form-7 id="224" title="Contactar ahora"]' ); ?>
        </div>

      </div>
    </div>
</section>
<?php } ?> 
  <section class="other">
    <div class="container">
      <p class="c-h2 text-center c-titi-sem">PROYECTOS EN VENTA</p>
      <div class="row c-mtop-xs">

        <?php 
          query_posts(array( 
          'post_type' => 'proyecto',
          'posts_per_page' => 3,
          'orderby' => 'rand',
          'proyectos' => 'actuales'
          ) ); 
          $j=0;
                         
          while (have_posts()) : the_post(); 
        ?>
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="<?php the_permalink(); ?>"><figure><img src="<?php the_post_thumbnail_url( 'full' ); ?>" alt=""></figure></a>
            <div class="c-proy-text">
              <a href="<?php the_permalink(); ?>" class="c-h2 c-color-sklight c-titi-sem"><?php the_title(); ?></a>
              <p class="c-h4 c-color-sklight c-titi"><i><?php echo get_field("direccion"); ?></i></p>
              <p><?php echo get_excerpt(125); ?></p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br><?php echo get_field("area"); ?>m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br><?php echo get_field("pisos"); ?> 
              </div>
              <div class="col-xs-4">
                DPTOS.<br><?php echo get_field("departamento"); ?> 
              </div>
            </div>
          </div>
        </div>
        <?php $j++; ?>
        <?php endwhile;?>
        <?php wp_reset_query(); ?>           

      </div>
    </div>
  </section>

<script>

  function moveCenter(latitud, longitud, index){
    var center = new google.maps.LatLng(latitud, longitud);
      // using global variable:
      map.panTo(center);
      //abre el popup del marcador
      //google.maps.event.trigger(markers[index], 'click');

  }
  var locations = [
    <?php foreach ($locations as $key => $value): ?>
        ['<?php echo get_field('icono', 'servicios_' . $value['serv']); ?>', <?php echo $value['lat'] ?>, <?php echo $value['long'] ?>, <?php echo $value['serv']; ?>],
    <?php endforeach ?>
      
  ];

  var markers = [];
  var map;
  function initMap() {
    //var myLatLng = {lat: -12.1275186, lng: -76.9753165};
    var myLatLng = {lat: <?php echo get_field("latitud"); ?>, lng: <?php echo get_field("longitud"); ?>};
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: <?php echo get_field("zoom"); ?>,
      center: myLatLng
    });
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: "<?php echo get_template_directory_uri(); ?>/img/marker.png",
      title: 'Visitanos',
      animation: google.maps.Animation.DROP,
    });

    

    

  }

  function showOnlyMarkers($id_serv){

    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    markers = [];

    //var infowindow = new google.maps.InfoWindow();
    for (var i = 0; i < locations.length; i++) {
            // Let's also add a marker while we're at it
        if ($id_serv == locations[i][3])  {

          var marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1],locations[i][2] ),
              map: map,
              animation: google.maps.Animation.DROP,
              icon:locations[i][0]
          });

          markers.push(marker);

        }
        //ACTIVA EL POP-UP INFORMATIVO DEL MARKADOR
        /*google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
            }
        })(marker, i));*/
        
    };

  }
  jQuery(document).ready(function($){

    $("a.view-markers").on("click", function(){
      
      $("a.view-markers").removeClass("active");
      $(this).addClass("active");

    })

  })
</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6w1tQpuDMZffD-cPP6y5cTXw42rHF_Ag&callback=initMap">
</script>

<?php /**********************FOOTER***************************/ ?>

            <?php get_footer(); ?>

<?php /**********************ENDFOOTER***************************/ ?>
