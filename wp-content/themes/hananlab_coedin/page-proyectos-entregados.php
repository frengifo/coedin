<?php
/*
Template Name:Proyectos Entregados
*/
?>   
        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>

  <section class="banner">
    <div class="c-elastic">
                  <?php query_posts(array( 
                                'post_type' => 'cabecera',
                                'name' => 'proyectos-entregados'

                            ) ); 
                          
                        while (have_posts()) : the_post(); 
                        ?>      
      <div class="c-mbot-sm c-img-bgcover c-coverslide" style="background-image: url(<?php echo get_field("imagen"); ?>);">
        <div class="c-box-text c-color-blackt1">
          <p class="c-h2 c-titi-bol"><?php echo get_field("titulo"); ?></p>
          <p class="c-h3 c-titi"><?php echo get_field("subtitulo"); ?></span></p>
        </div>
      </div>
                        <?php endwhile;?>
                        <?php wp_reset_query(); ?>         
    </div>
  </section>

  <section class="other">
    <div class="container">
      <div class="row c-mtop-xs">


        <?php 
              $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
              query_posts('post_type=proyecto&post_status=publish&posts_per_page=6&estado=entregado&paged='.$paged); ?>

        <?php while ( have_posts() ) : the_post(); ?>
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="<?php the_permalink(); ?>"><figure><img src="<?php the_post_thumbnail_url( 'full' ); ?>" alt=""></figure></a>
            <div class="c-proy-text">
              <a href="<?php the_permalink(); ?>" class="c-h2 c-color-sklight c-titi-sem"><?php the_title() ?></a>
              <p class="c-h4 c-color-sklight c-titi"><i><?php echo get_field("direccion"); ?></i></p>
              <p><?php echo get_excerpt(125); ?></p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br><?php echo get_field("area"); ?>m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br><?php echo get_field("pisos"); ?> 
              </div>
              <div class="col-xs-4">
                DPTOS.<br><?php echo get_field("departamento"); ?> 
              </div>
            </div>
          </div>
        </div>        
        <?php endwhile; ?>

      </div>
    </div>
  </section>

  <section>
    <div class="container text-center">
      <!--
      <ul class="list-inline c-paginator c-titi">        
        <li><a href="#"><i class="fa fa-caret-left"></i></a></li>
        <li><a href="#">1</a></li>
        <li><a href="#" class="activo">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#"><i class="fa fa-caret-right"></i></a></li>
      </ul>
    -->
                            <?php 
/*
                                the_posts_pagination( array(
                                    'mid_size'  => 2,
                                    'screen_reader_text' => ' ', 
                                    'prev_text' => __( '&#171; Anterior', 'proyecto' ),
                                    'next_text' => __( 'Siguiente &#187;', 'proyecto' ),
                                ) );
*/
                                the_posts_pagination( array(
                                    'mid_size'  => 2,
                                    'screen_reader_text' => ' ', 
                                    'prev_text' => __( '<i class="fa fa-caret-left" aria-hidden="true" style="color: #0f9ec7; font-size: 1.5em;"></i>', 'proyecto' ),
                                    'next_text' => __( '<i class="fa fa-caret-right" aria-hidden="true" style="color: #0f9ec7; font-size: 1.5em;"></i>', 'proyecto' ),
                                ) );
                             ?>    
    </div>
  </section>

  
<?php /**********************FOOTER***************************/ ?>

            <?php get_footer(); ?>

<?php /**********************ENDFOOTER***************************/ ?>