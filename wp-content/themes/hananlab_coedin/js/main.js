jQuery(document).ready(function($){
  $(".c-mbot-xs a.c-titi-sem").on("click",function(){
    var id=$(this).attr("href");
    console.log(id);
    $(".c-mbot-xs a").removeClass('activo');
    $(this).addClass('activo');
    $(".tab-pane").removeClass("active");
    $(".tab-pane").removeClass("in");
    $(id).addClass("active");
    $(id).addClass("in");
    //alert("heble");
    return false;
  })

  //Slider
  var slider = $(".slider-box").unslider({

    nav : true,
    arrows: false,
    autoplay:true,
    delay:6000
  });  

  /*PRODUCTO DETALLE*/
  $('.c-proy-detaimg .full').slick({
  
    arrows: false,
    asNavFor: '.c-proy-detaimg .thumb',
    slidesToShow: 1,
    fade: true,
  });
  $('.c-proy-detaimg .thumb').slick({
    arrows: false,
    vertical:true,
    asNavFor: '.c-proy-detaimg .full',
    slidesToShow: 5,
    slidesToScroll: 1,
    focusOnSelect: true,
    responsive: [
    
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1
      }
    }
      
    ]
  });

  $(".navbar-toggle").on("click", function(){
    $("#menu-menu-principal").slideToggle();
  })
  /*DEPARTAMENTOS*/
  $(".hover-proyectos .c-img-bgcover")
  .mouseenter(function() {
    $(this).parent().find(".c-img-bgcover").removeClass("this");
    $(this).parent().find(".c-img-bgcover").addClass("rep");

    $(this).removeClass("rep");
    $(this).addClass("this");

  })
  .mouseleave(function() {
    $(this).parent().find(".c-img-bgcover").removeClass("this");
    $(this).parent().find(".c-img-bgcover").removeClass("rep");
  });

  $(".c-tipo-btns .c-iblock").each(function(index){
    var dep = $(this).find("a").attr("href");
    $(dep+' .full').slick({
    
        arrows: false,
        asNavFor: dep+' .thumb',
        slidesToShow: 1,
        fade: true,
      });
      $(dep+' .thumb').slick({
        arrows: true,
        asNavFor: dep+' .full',
        slidesToShow: 5,
        slidesToScroll: 1,
        focusOnSelect: true,
        responsive: [
        
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            vertical:true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            vertical:true
          }
        }
          
        ]
      });

  });

  if ( $(".various").length > 0 ) {
    $(".various").fancybox({
      maxWidth  : 500,
      fitToView : false,
      width   : '70%',
      padding: 0,
      autoSize  : true,
      closeClick  : false,
      openEffect  : 'none',
      closeEffect : 'none'
    });
  };

});


$('#frmContacto').submit(function(e) { 
   e.preventDefault();
   
   if ( $(this).parsley().isValid() ) {
      //alert("enviar!");

      jQuery.get( url_site + '/wp-admin/admin-ajax.php', {
          
           action: "register_contact",
           inversionista: $(this).find("input[name=inversionista]:checked").val(),
           nombres: $(this).find("input[name='nombres']").val(),
           apellidos: $(this).find("input[name='apellidos']").val(),
           mail: $(this).find("input[name='mail']").val(),
           telefono: $(this).find("input[name='telefono']").val(),
           tipo: $(this).find("input[name=tipo]:checked").val(),
           otro: $(this).find("input[name='otro']").val(),
           proyecto: $(this).find("select[name='proyecto']").val(),
           mensaje: $(this).find("textarea[name='mensaje']").val(),
           mensaje: $(this).find("checkbox[name='acepto']").val(),
           

         }, function(data){
             
           if (data.success) {
             
             alert('Mensaje enviado con éxito');

           }else{
             
              alert('Ocurrio un error al enviar, por favor intentelo nuevamente');
           }

         }, 'json');


   }
});