<?php
/*
Template Name: Search Page
*/
?>
        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>
  <style type="text/css">
    
  .grid-sizer{
    width: 25%;
  }
  .c-mbot-sm{
    height: 580px;
  }
.c-notihome {
    position: relative;
    height: 100%;
    display: block;
    background: #f4f4f4;
    width: 100%;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 50% 50%;

}
  .c-notihome .c-noti-text {
    position: absolute;
    z-index: 10;
    padding: 1em;
    width: 100%;
    left: 0;
    bottom: 0;
    text-shadow:none;
    background-color: rgba(244, 244, 244, 1);
  }


  @media screen and (max-width: 1200px) {
    .c-mbot-sm{
      font-size: 1em;
      height: 480px;
    }
  }
  @media screen and (max-width: 768px) {
    /* 5 columns for larger screens */
    .grid-sizer { width: 50%; }
    .c-mbot-sm{
      height: 430px;
    }
  }

   @media screen and (max-width: 480px) {
    /* 5 columns for larger screens */
    .grid-sizer, .c-mbot-sm { width: 100%; }
    .c-mbot-sm{
      height: 400px;
    }
  }
  </style>
  <section>
    <div class="container">
      <h2> <span></span> Busqueda con la palabra: "<?php echo $_GET['search']; ?>"</h2>
      <div class="row c-mtop-sm" style="position:relative;">
        <div class="grid-sizer"></div>
                  <?php 

                  $args = array(  
          'post_status' => 'publish',
          'post_type' => 'post',
          's' =>  isset($_GET['search']) ? $_GET['search']:"" );

                  $search = new WP_Query($args);
                  ?>
                  <?php $i=1; ?>
                   <?php while ( $search->have_posts() ) : $search->the_post(); ?>
<?php if (get_field("columna")=="1") { ?>

                  <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
                    <div class="c-notihome c-notipo2 c-img-bgcover" style="background-image: url(<?php the_post_thumbnail_url( 'full' ) ?>);">
                      <div class="c-noti-text c-color-blackt1 c-text-shadow2" style="background-color:<?php echo hex2rgba(get_field('color_fondo'),get_field('opacidad')) ?>">
                        <p><small><?php echo get_the_date(); ?></small></p>
                        <!-- <h4><?php the_title(); ?></h4> -->
                        <div><?php echo get_field('descripcion'); ?></div>
                        <div class="clearfix">
                          <a href="<?php the_permalink(); ?>" class="btn c-bor-gray pull-right">Leer más</a>
                        </div>
                      </div>
                    </div>
                  </div>  

<?php } ?>

<?php if (get_field("columna")=="2") { ?>
        <div class="col-sm-6 col-md-6 c-mbot-sm col-xs-6">
                    <div class="c-notihome c-notipo2 c-img-bgcover" style="background-image: url(<?php the_post_thumbnail_url( 'full' ) ?>);">
                      <div class="c-noti-text c-color-blackt1 c-text-shadow2" style="background-color:<?php echo hex2rgba(get_field('color_fondo'),get_field('opacidad')) ?>">
                        <p><small><?php echo get_the_date(); ?></small></p>
                        <!-- <h4><?php the_title(); ?></h4> -->
                        <div><?php echo get_field('descripcion'); ?></div>
                        <div class="clearfix">
                          <a href="<?php the_permalink(); ?>" class="btn c-bor-gray pull-right">Leer más</a>
                        </div>
                      </div>
                    </div>
        </div>
<?php  } ?>
    
                        <?php $i++; ?>
                   <?php endwhile; ?>
 


      </div>
    </div>
  </section>
<section>
                    <div class="container">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center; font-size: 20px; ">
                            <?php 

                                the_posts_pagination( array(
                                    'mid_size'  => 2,
                                    'screen_reader_text' => ' ', 
                                    'prev_text' => __( '<i class="fa fa-caret-left" aria-hidden="true" style="color: #0f9ec7; font-size: 1.5em;"></i>', 'proyecto' ),
                                    'next_text' => __( '<i class="fa fa-caret-right" aria-hidden="true" style="color: #0f9ec7; font-size: 1.5em;"></i>', 'proyecto' ),
                                ) );

                             ?>
                        </div>
                    </div>      
                   </div> 
</section>
  
<?php get_footer('noticias'); ?>