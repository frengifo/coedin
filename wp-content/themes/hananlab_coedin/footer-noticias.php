    <footer class="c-bg-blackt1">
      <div class="container">
                  <?php query_posts(array( 
                                'post_type' => 'page',
                                'name' => 'pie-de-pagina'

                            ) ); 
                          
                        while (have_posts()) : the_post(); 
                        ?>         
        <div class="row c-mtop-md">
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <p class="c-h4 c-titi-sem">LA EMPRESA</p>
            <div class="c-text-justify c-mbot-xs c-lato-lig c-lheight-lg">
              <?php echo get_field('empresa'); ?>
            </div>
            <a href="#" class="c-link">LEER MÁS</a>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <p class="c-h4 c-titi-sem">ATENCIÓN AL CLIENTE</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="<?php echo site_url() ?>/terminos-y-condiciones/">Términos y Condiciones</a></li>
              <li><a href="<?php echo site_url() ?>/politicas-de-privacidad/">Políticas de Privacidad</a></li>
            </ul>
            <p class="c-h4 c-titi-sem">TIPOS DE INVERSIÓN</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="#">Venta de terrenos</a></li>
              <li><a href="#">Compra de casas y departamentos</a></li>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <div class="c-foot-invierte">
              <p class="c-h3 c-color-sklight c-titi">INVIERTE CON <br><span class="c-h2 c-lheight-sm">COEDIN</span></p>
              <div class="c-text-justify c-lato-lig">
                <small class="c-color-white"><?php echo get_field('invierte_coedin'); ?></small>
              </div>
              <p class="c-mtop-xs"><a href="#" class="btn btn-block c-bg-sklight c-titi">INVIERTE<br><span class="c-h4 c-titi-sem">AHORA</span></a></p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md c-fdatos">
            <p class="c-h4 c-titi-sem">CONTÁCTANOS</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="https://goo.gl/maps" target="_blank"><i class="fa fa-map-marker c-color-white"></i> <span><?php echo get_field('direccion') ?></span></a></li>
              <li><a href="mailto:<?php echo get_field('mail') ?>"><i class="fa fa-envelope c-color-white"></i> <span><?php echo get_field('mail') ?></span></a></li>
            </ul>
            <p class="c-h4 c-titi-sem">SÍGUENOS EN</p>
            <div class="c-redes c-mtop-xs">
              <div class="c-iblock">
                <a href="<?php echo get_field('facebook') ?>" target="_blank"><i class="fa fa-facebook"></i> </a>
              </div>
              <div class="c-iblock">
                <a href="<?php echo get_field('youtube') ?>"><i class="fa fa-youtube-play"></i> </a>
              </div>
              <div class="c-iblock">
                <a href="<?php echo get_field('linkedin') ?>"><i class="fa fa-linkedin"></i> </a>
              </div>

            </div>
          </div>
        </div>
                        <?php endwhile;?>
                        <?php wp_reset_query(); ?>  

      </div>

      <div class="c-foot c-bg-blackt1">
        <div class="container text-center">
          <small class="c-color-sklight">Copyright &copy; 2016 <strong>Coedin</strong> | Diseñado y Desarrollado por <strong>Reder Design</strong></small>
        </div>
      </div>
    </footer>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://npmcdn.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
    <script type="text/javascript">
      $(function() {
        $('.c-menu a').each(function() {
          var word = $(this).html();
          var index = word.indexOf(' ');
          if(index > -1) {
            // index = word.length;
            $(this).html(word.substring(0, index) + '<br>' + word.substring(index, word.length));
          }
        });

        $('.carousel').carousel({
          interval: 7000
        })
        /**/
        $('#miniCarousel').carousel({
          interval: 10000
        });
        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click( function(){
            var id = this.id.substr(this.id.lastIndexOf("-") + 1);
            var id = parseInt(id);
            $('#miniCarousel').carousel(id);
        });
 
      });
    </script>
    <script type="text/javascript">
      
      jQuery(document).ready(function(){

          $('.c-mtop-sm').masonry({
            // options...
            itemSelector: '.c-mbot-sm',
            columnWidth: '.grid-sizer',
            percentPosition: true
          });

      })

  </script>
    <?php wp_footer(); ?>   
  </body>
</html>