<?php
/*
Template Name:Proyectos Actuales
*/
?>   
        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>

  <section class="banner">
    <div class="c-elastic">
                  <?php query_posts(array( 
                                'post_type' => 'cabecera',
                                'name' => 'proyectos-actuales'

                            ) ); 
                          
                        while (have_posts()) : the_post(); 
                        ?>      
      <div class="c-mbot-sm c-img-bgcover c-coverslide" style="background-image: url(<?php echo get_field("imagen"); ?>);">
        <div class="c-box-text c-color-blackt1">
          <p class="c-h2 c-titi-bol"><?php echo get_field("titulo"); ?></p>
          <p class="c-h3 c-titi"><?php echo get_field("subtitulo"); ?></span></p>
        </div>
      </div>
                        <?php endwhile;?>
                        <?php wp_reset_query(); ?>         
    </div>
  </section>
<?php 
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    query_posts('post_type=proyecto&post_status=publish&posts_per_page=4&estado=actual&paged='.$paged); 

    $j=0;  

    while (have_posts()) : the_post(); 

?> 


      <section class="project">
        <div class="container c-proyslide">
          
          <div class="view-project">
            <a href="<?php the_permalink(); ?>" class="hover"  style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>')" >
              <figure class="c-proyslide-img">
                <figcaption class="c-mask-skylight text-center c-color-white">
                  <div class="c-mask-text">
                    <p class="c-h2 c-titi-bol"><?php the_title(); ?></p>
                    <p class="c-h4 c-titi"><?php echo get_field("distrito"); ?></p>
                  </div>
                </figcaption>
              </figure>
            </a>
            <div class="row">
              
              

              <div class="col-sm-12 col-md-12  text-left" style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>')">
                <div class="c-infoproy">
                  <span class="c-h1 c-block c-titi-sem"><?php the_title(); ?></span>
                  <p class="c-h3 c-titi-sem"><?php echo get_field("distrito"); ?></p>
                  <div class="c-desc">
                    <p><?php the_content(); ?></p>
                  </div>
                  <div class="c-features c-color-sklight c-titi-sem text-center c-mtop-sm c-mbot-sm">
                    <div class="c-iblock-container">
                      <div class="c-iblock">
                        ÁREA<br><?php echo get_field("area"); ?>m<sup>2</sup> 
                      </div>
                      <div class="c-iblock">
                        PISOS<br><?php echo get_field("pisos"); ?> 
                      </div>
                      <div class="c-iblock">
                        DPTOS.<br><?php echo get_field("departamento"); ?> 
                      </div>
                    </div>
                  </div>
                  <div class="text-center">
                    <a href="<?php the_permalink(); ?>" class="btn c-bg-sklight c-color-white">VER MÁS</a>
                  </div>
                </div>
              </div> 

            </div>


          </div>          
        </div>
      </section>



<?php $j++; ?>
<?php endwhile;?>
<?php wp_reset_query(); ?>   

  <section>
    <div class="container text-center">
                            <?php 

                                the_posts_pagination( array(
                                    'mid_size'  => 2,
                                    'screen_reader_text' => ' ', 
                                    'prev_text' => __( '&#171; Anterior', 'proyecto' ),
                                    'next_text' => __( 'Siguiente &#187;', 'proyecto' ),
                                ) );

                             ?>    
    </div>
  </section>        
  
<?php /**********************FOOTER***************************/ ?>

            <?php get_footer(); ?>

<?php /**********************ENDFOOTER***************************/ ?>