<?php 
/*
function wp_rest_api_alter() {
  register_api_field( 'post',
      'fields',
      array(
        'get_callback'    => function($data, $field, $request, $type){
          if (function_exists('get_fields')) {
            return get_fields($data['id']);
          }
          return [];
        },
        'update_callback' => null,
        'schema'          => null,
      )
  );
}
add_action( 'rest_api_init', 'wp_rest_api_alter');
*/

/***************************COMENTARIOS************************************************************/
function mytheme_comment($comment, $args, $depth) { ?>


            <div class="row">
              <div class="col-sm-2 col-md-2 c-mbot-xs c-userpic">
                <figure><?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?></figure>
              </div>
              <div class="col-sm-10 col-md-10 c-mbot-sm">
                <span class="c-h4 c-titi-sem c-block"><?php comment_author(); ?></span>
                <span class="c-mbot-xs c-block"><?php

              printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );

            ?></span>
                <span class="pull-right"><?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></span>
                <p><small><?php comment_text(); ?></small></p>
              </div>
            </div>   


    <div class="clear"></div>

    <?php if ( $comment->comment_approved == '0' ) : ?>

         <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>

          <br />

    <?php endif; 



    }
/*************************** FIN DE COMENTARIOS************************************************************/


/********** START: CONTACT FORM **********/
wpcf7_add_shortcode('postdropdown', 'createbox', true);
function createbox(){
  global $post;
  extract( shortcode_atts( array( 'post_type' => 'proyecto',), $atts ) );
  $args = array('post_type' => $post_type );
  $myposts = get_posts( $args );
  $output = "<select name='proyecto' id='postType' onchange='document.getElementById(\"postType\").value=this.value;' class='form-control'><option value=''>Selecciona un proyecto</option>";
  foreach ( $myposts as $post ) : setup_postdata($post);
    $title = get_the_title();
    $output .= "<option value='$title'> $title </option>";

  endforeach;
  $output .= "</select>";
  return $output;
}
/**********  END: CONTACT FORM  **********/


/******COLOR DE FONDO******/

function hex2rgba($color, $opacity = false) {
 
  $default = 'rgb(0,0,0)';
 
  //Return default if no color provided
  if(empty($color))
          return $default; 
 
  //Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
          $color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
          if(abs($opacity) > 1)
            $opacity = 1.0;
          $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
          $output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}

/******FIN DE COLOR DE FONDO******/


function get_excerpt($count){
  //$permalink = get_permalink($post->ID);
  $excerpt = get_the_content();
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = $excerpt.'...';
  //$excerpt = $excerpt.'... <a href="'.$permalink.'">more</a>';
  return $excerpt;
}

function ajax_register_contact() {

global $wpdb;
$response = array('success' => true, 'message' => 'Gracias por registrarte.' );
//$email = $wpdb->get_results( "SELECT ID FROM contactos where email = '".$_GET['email']."' limit 0,1" );

 
  $wpdb->insert( 
  'contactos', 
    array( 
      'inversionista' => $_GET['inversionista'],      
      'nombres' => $_GET['nombres'] ,
      'apellidos' => $_GET['apellidos'] ,      
      'mail' => $_GET['mail'] ,              
      'telefono' => $_GET['telefono'] ,
      'tipo' => $_GET['tipo'] ,
      'otro' => $_GET['otro'] ,
      'proyecto' => $_GET['proyecto'] ,
      'mensaje' => $_GET['mensaje'] ,      
      'mobile' => wp_is_mobile() ? 1: 0,
    )
  );

  $to = 'rnz-silva@hotmail.com';  
  $subject = 'COEDIN / Invierte / Nuevo registro ';
  $body = 'Mensaje';
  $headers = array('Content-Type: text/html; charset=UTF-8');

  wp_mail( $to, $subject, $body, $headers ); 


echo json_encode( $response);

wp_die();

} 

function get_the_slug( $id=null ){
  if( empty($id) ):
    global $post;
    if( empty($post) )
      return ''; // No global $post var available.
    $id = $post->ID;
  endif;

  $slug = basename( get_permalink($id) );
  return $slug;
}

/************ START: FUNCION REGISTRAR MENU *****************/

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

/************ END: FUNCION REGISTRAR MENU *****************/


/************ START: FUNCION CAMBIAR LOGO *****************/

add_action('admin_head', 'my_custom_logo');
function my_custom_logo() {
echo '
<style type="text/css">
#wp-admin-bar-wp-logo > .ab-item .ab-icon { background-image: url('.get_bloginfo('template_directory').'/images/logo-cliente.png) !important; }
</style>
';
}

/************ END: FUNCION CAMBIAR LOGO *****************/


/************ START: FUNCION CREAR ESPACIOS PARA WIDGETS *****************/

if ( function_exists('register_sidebar') )
{
register_sidebar(array('name' => 'Footer Left','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));
register_sidebar(array('name' => 'Footer Center','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));
register_sidebar(array('name' => 'Footer Right','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));

register_sidebar(array('name' => 'Sub Footer','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));
register_sidebar(array('name' => 'Footer Info','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));
}


/************ END: FUNCION CREAR WIDGETS *****************/

function miplugin_register_sidebars(){
    register_sidebar(array(
        "name" => "Barra derecha",
        "id" => "id-sidebar-derecha",
        "descripcion" => "Barra de la parte derecha",
        "class" => "sidebar_widget",
        "before_widget" => "<li id='%1$s' class='%2$s'>",
        "after_widget" => "</li>",
        "before_title" => "<h2 class='titulodelwidget'>",
        "after_title" => "</h2>"
    ));
}
add_action('widgets_init','miplugin_register_sidebars');


function miplugin_register_sidebars_noticias(){
    register_sidebar(array(
        "name" => "Barra noticias",
        "id" => "id-sidebar-noticias",
        "descripcion" => "Barra de la parte derecha de noticias",
        "class" => "sidebar_widget",
        "before_widget" => "",
        "after_widget" => "<br>",
        "before_title" => "<h3 class='widget-title'>",
        "after_title" => "</h3>"
    ));
}
add_action('widgets_init','miplugin_register_sidebars_noticias');

/*
// Borrar opciones de admin
 
add_action('admin_menu', 'my_remove_menu_pages');
 
  function my_remove_menu_pages() {
            remove_menu_page('edit.php'); // Entradas
            remove_menu_page('upload.php'); // Multimedia
            remove_menu_page('link-manager.php'); // Enlaces
            remove_menu_page('edit.php?post_type=page'); // Páginas
            remove_menu_page('edit-comments.php'); // Comentarios
            remove_menu_page('themes.php'); // Apariencia
            remove_menu_page('plugins.php'); // Plugins
            remove_menu_page('users.php'); // Usuarios
            remove_menu_page('tools.php'); // Herramientas
            remove_menu_page('options-general.php'); // Ajustes
  }
  */


/************ START: FUNCION PERSONALIZAR IMAGEN DE CABECERA *****************/

$defaults = array(
  'default-image'          => '',
  'width'                  => 0,
  'height'                 => 0,
  'flex-height'            => false,
  'flex-width'             => false,
  'uploads'                => true,
  'random-default'         => false,
  'header-text'            => true,
  'default-text-color'     => '',
  'wp-head-callback'       => '',
  'admin-head-callback'    => '',
  'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );

/************ END: FUNCION PERSONALIZAR IMAGEN DE CABECERA *****************/


/************ START: FUNCION MOSTRAR IMAGEN DESTACADA EN POST *****************/
add_theme_support( 'post-thumbnails' );
/************ END: FUNCION MOSTRAR IMAGEN DESTACADA EN POST *****************/


/************ END: FUNCION MOSTRAR PORTFOLIO *****************/

/*--- Custom Messages - project_updated_messages ---*/
add_filter('post_updated_messages', 'project_updated_messages');
 
function project_updated_messages( $messages ) {
  global $post, $post_ID;
 
  $messages['project'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Project updated. <a href="%s">View project</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Project updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Project restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Project published. <a href="%s">View project</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Project saved.'),
    8 => sprintf( __('Project submitted. <a target="_blank" href="%s">Preview project</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Project scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview project</a>'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Project draft updated. <a target="_blank" href="%s">Preview project</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
 
  return $messages;
}
 
/*--- #end SECTION - project_updated_messages ---*/

/*--- Demo URL meta box ---*/
 
add_action('admin_init','portfolio_meta_init');
 
function portfolio_meta_init()
{
    // add a meta box for WordPress 'project' type
    add_meta_box('portfolio_meta', 'Project Infos', 'portfolio_meta_setup', 'project', 'side', 'low');
  
    // add a callback function to save any data a user enters in
    add_action('save_post','portfolio_meta_save');
}
 
function portfolio_meta_setup()
{
    global $post;
      
    ?>
        <div class="portfolio_meta_control">
            <label>URL</label>
            <p>
                <input type="text" name="_url" value="<?php echo get_post_meta($post->ID,'_url',TRUE); ?>" style="width: 100%;" />
            </p>
        </div>
    <?php
 
    // create for validation
    echo '<input type="hidden" name="meta_noncename" value="' . wp_create_nonce(__FILE__) . '" />';
}
 
function portfolio_meta_save($post_id) 
{
    // check nonce
    if (!isset($_POST['meta_noncename']) || !wp_verify_nonce($_POST['meta_noncename'], __FILE__)) {
    return $post_id;
    }
 
    // check capabilities
    if ('post' == $_POST['post_type']) {
    if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
    }
    } elseif (!current_user_can('edit_page', $post_id)) {
    return $post_id;
    }
 
    // exit on autosave
    if (defined('DOING_AUTOSAVE') == DOING_AUTOSAVE) {
    return $post_id;
    }
 
    if(isset($_POST['_url'])) 
    {
        update_post_meta($post_id, '_url', $_POST['_url']);
    } else
    {
        delete_post_meta($post_id, '_url');
    }
}

function coedin_scripts() {


    if ( is_singular( 'proyecto' ) ) {

    // Add Genericons, used in the main stylesheet.

    wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.css', array(), '2.1.5' );

    wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.pack.js', array(), '2.1.5');

    // Get the post type from the query

  }

}
add_action( 'wp_enqueue_scripts', 'coedin_scripts' );

/*--- #end  Demo URL meta box ---*/

function enqueue_filterable() 
{
    /*wp_register_script( 'filterable', get_template_directory_uri() . '/js/filterable.js', array( 'jquery' ) );
    wp_enqueue_script( 'filterable' );*/
}
add_action('wp_enqueue_scripts', 'enqueue_filterable');


// Get menus to play nicely with the submenu script
// blissfully borrowed from Post Type Archive Links plugin, thanks @stephenharris, @F J Kaiser, @ryancurban
function mrw_tax_archive_current( $items ) {
    foreach ( $items as $item ) {
        if ( 'taxonomy' !== $item->type )
            continue;

        global $post;

        if( !$post )
            continue;

        $taxonomy = $item->object;
        $taxonomy_term = $item->object_id;
        if (
            ! is_tax( $taxonomy, $taxonomy_term )
            AND ! has_term( $taxonomy_term, $taxonomy, $post->ID )
        )
            continue;

        // Make item current
        $item->current = true;
        $item->classes[] = 'current-menu-item';

        // Loop through ancestors and give them 'parent' or 'ancestor' class
        $active_anc_item_ids = mrw_get_item_ancestors( $item );
        foreach ( $items as $key => $parent_item ) {
            $classes = (array) $parent_item->classes;

            // If menu item is the parent
            if ( $parent_item->db_id == $item->menu_item_parent ) {
                $classes[] = 'current-menu-parent';
                $items[ $key ]->current_item_parent = true;
            }

            // If menu item is an ancestor
            if ( in_array( intval( $parent_item->db_id ), $active_anc_item_ids ) ) {
                $classes[] = 'current-menu-ancestor';
                $items[ $key ]->current_item_ancestor = true;
            }

            $items[ $key ]->classes = array_unique( $classes );
        }
    }

    return $items;
}
add_filter('wp_nav_menu_objects','mrw_tax_archive_current');

function mrw_get_item_ancestors( $item ) {
    $anc_id = absint( $item->db_id );

    $active_anc_item_ids = array();
    while (
        $anc_id = get_post_meta( $anc_id, '_menu_item_menu_item_parent', true )
        AND ! in_array( $anc_id, $active_anc_item_ids )
    )
        $active_anc_item_ids[] = $anc_id;

    return $active_anc_item_ids;
}

/**FILTRAR POR META KEY */


add_action( 'add_meta_boxes', 'add_departamento_meta' );
 
  /* Saving the data */
  add_action( 'save_post', 'departamento_meta_save' );

/* Adding the main meta box container to the post editor screen */
  function add_departamento_meta() {
      add_meta_box(
          'seleccionar-proyectos',
         'Seleccione Proyecto',
          'departamento_details_init',
          'departamento');
  }

function departamento_details_init() {
  global $post;
  // Use nonce for verification
  wp_nonce_field( plugin_basename( __FILE__ ), 'departamento_nonce' );

  $proyectos =  get_posts(array('post_type' => 'proyecto' ) );
  $proyecto_rel = get_post_meta($post->ID,'proyecto_rel',true);
  //var_dump($proyecto_rel);
  ?>
  <select name="proyecto_rel" required>
      <option value="" ><?php _e('[Proyecto]', 'baapf'); ?></option>
      <?php
          
         foreach ( $proyectos as $post ) : setup_postdata($post); ?>

          <option value="<?php echo get_the_id() ?>" <?php echo $proyecto_rel == get_the_id() ? 'selected="selected"':''; ?> > <?php echo get_the_title(); ?></option>

      <?php endforeach;
    ?>
    </select>
    <?php

}

function departamento_meta_save( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
        return;
    // Verifying the nonce
    if ( !isset( $_POST['departamento_nonce'] ) )
        return;

    if ( !wp_verify_nonce( $_POST['departamento_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Updating the employeeDetails meta data
    $employeeDetails = $_POST['proyecto_rel'];

    update_post_meta($post_id,'proyecto_rel',$employeeDetails);
}


add_action( 'add_meta_boxes', 'add_employee_meta' );
 
  /* Saving the data */
  add_action( 'save_post', 'employee_meta_save' );

  /* Adding the main meta box container to the post editor screen */
  function add_employee_meta() {
      add_meta_box(
          'employee-details',
         'Servicios cercanos',
          'employee_details_init',
          'proyecto');
  }

  /*Printing the box content */
  function employee_details_init() {
      global $post;
      // Use nonce for verification
      wp_nonce_field( plugin_basename( __FILE__ ), 'employee_nonce' );

      $servicios = get_terms( array(
          'taxonomy' => 'servicios',
          'hide_empty' => false,
      ) );

      
      
      ?>
      <div id="employee_meta_item">
      <?php

      //Obtaining the linked employeedetails meta values
      $employeeDetails = get_post_meta($post->ID,'employeeDetails',true);
      $c = 0;
      if ( count( $employeeDetails ) > 0 && is_array($employeeDetails)) {
          foreach( $employeeDetails as $employeeDetail ) {
              if ( isset( $employeeDetail['lat'] ) || isset( $employeeDetail['long'] ) ) {

                  $select = '<select name="employeeDetails[%1$s][serv]">';
                  foreach ($servicios as $key => $value) {

                    if ( $employeeDetail['serv'] == $value->term_id ) {
                      $select .= '<option value="'.$value->term_id.'" selected="selected">'.$value->name.'</option>';
                    }else{
                      $select .= '<option value="'.$value->term_id.'">'.$value->name.'</option>';
                    }
                  }

                  $select .='</select>';

                  printf( '<p>Latitud<input type="text" name="employeeDetails[%1$s][lat]" value="%2$s" /> Longitud : <input type="text" name="employeeDetails[%1$s][long]" value="%3$s">'.$select.'<a href="javascript:;" class="remove-package">%4$s</a></p>', $c, $employeeDetail['lat'], $employeeDetail['long'], 'Remove' );
                  $c = $c +1;
              }
          }
      }

      $selectHTML = '<select name="employeeDetails[\'+count+\'][serv]">';
      foreach ($servicios as $key => $value) {

        $selectHTML .= '<option value="'.$value->term_id.'">'.$value->name.'</option>';

      }

      $selectHTML .='</select>';

      ?>
      <span id="output-package"></span>
      <a href="javascript:;" class="add_package"><?php _e('Agregar Servicio'); ?></a>
      <script>
          var $ =jQuery.noConflict();
          $(document).ready(function() {
              var count = <?php echo $c; ?>;
              $(".add_package").click(function() {
                  count = count + 1;

                  $('#output-package').append('<p> Latitud <input type="text" name="employeeDetails['+count+'][lat]" value="" />  Longitud : <input type="text" name="employeeDetails['+count+'][long]" value=""><?php echo $selectHTML; ?><a href="javascript:;" class="remove-package">Remove</a></p>' );
                  return false;
              });
             $(document.body).on('click','.remove-package',function() {
                  $(this).parent().remove();
              });
          });
          </script>
      </div><?php

  }

  /* Save function for the entered data */
  function employee_meta_save( $post_id ) {
      if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
          return;
      // Verifying the nonce
      if ( !isset( $_POST['employee_nonce'] ) )
          return;

      if ( !wp_verify_nonce( $_POST['employee_nonce'], plugin_basename( __FILE__ ) ) )
          return;
      // Updating the employeeDetails meta data
      $employeeDetails = $_POST['employeeDetails'];

      update_post_meta($post_id,'employeeDetails',$employeeDetails);
  }

/**FILTRAR POR META KEY */

function restrict_departamento_by_issue() {
    global $post;
    global $typenow;
    if ( $typenow == 'departamento' ) {
    $proyectos =  get_posts(array('post_type' => 'proyecto' ) );
    ?>
    
    <select name="proyecto_rel">
        <option value="" ><?php _e('[Seleccione Proyecto]', 'baapf'); ?></option>
        <?php
            
           foreach ( $proyectos as $post ) : setup_postdata($post); ?>

            <option value="<?php echo get_the_id() ?>" <?php echo isset($_GET['proyecto_rel']) && $_GET['proyecto_rel'] == get_the_id() ? 'selected="selected"':''; ?> > <?php echo get_the_title(); ?></option>

        <?php endforeach;
      ?>
      </select>
      <?php
      }
} 
add_action('restrict_manage_posts','restrict_departamento_by_issue');



function posts_where( $where ) {
    if( is_admin() ) {
        global $wpdb;       
        if ( isset( $_GET['proyecto_rel'] ) && !empty( $_GET['proyecto_rel'] ) && intval( $_GET['proyecto_rel'] ) != 0 ) {
            $issue_number = intval( $_GET['proyecto_rel'] );

            $where .= " AND ID IN (SELECT post_id FROM " . $wpdb->postmeta ." 
WHERE meta_key='proyecto_rel' AND meta_value=$issue_number )";
        }
    }   
    return $where;
}
add_filter( 'posts_where' , 'posts_where' );