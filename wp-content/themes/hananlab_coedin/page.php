<?php
/*

*/
?>   
        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>

  <section>
    <div class="container">
      <h2><b><?php the_title() ?></b></h2>

      <div class="col-md-12" style="padding-left: 5%; padding-right: 5%;">
        <br>
        <?php the_content(); ?>
      </div>
    </div>
  </section>



<?php /**********************FOOTER***************************/ ?>



            <?php get_footer(); ?>



<?php /**********************ENDFOOTER***************************/ ?>